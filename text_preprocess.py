import re
from string import digits
import pickle
from keras_preprocessing.sequence import pad_sequences
from nltk.tokenize import word_tokenize
import nltk
nltk.download('punkt')

FORMAL_INFORMAL_DS_PATH = "../dataset/"

def remove_link(text):
  return re.sub(r'http\S+', '', text)

def remove_html_tag(text):
  return re.sub(re.compile('<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});'), 
                '', 
                text)

def casefolding(text):
  return text.lower()

def replace_new_line_with_space(text):
  return text.replace('\n', " ") 

def remove_emoji(text):
  return text.encode('ascii', 'ignore').decode('ascii')

def remove_extra_spaces(string):
  return " ".join(string.split())

def remove_special_char(text):
  return re.sub('[^A-Za-z0-9 ]+', '', text)

def remove_number(text):
  remove_digits = str.maketrans('', '', digits)
  return text.translate(remove_digits)

def removeNonAscii(text):
  return ''.join(char for char in text if ord(char) < 128)

def fix_review_with_punctuation(text):
  text = re.sub('([.,!?()])', r' \1 ', text)
  text = re.sub('\s{2,}', ' ', text)
  
  return text

# import formal_informal_words from pkl file in util dir
def get_formal_informal_words():
  formal_informal_words = None

  with open('static/pickle/formal_informal_words.pkl', 'rb') as file:
    formal_informal_words = pickle.load(file)

  return formal_informal_words

def convertNonStandardWords(review):
  checker = get_formal_informal_words()
  words = review.split()
    
  for idx, word in enumerate(words):
    if word in checker:
      words[idx] = checker[word]

  return ' '.join(words)

# import stopword_remover from pkl file in util dir
def get_stopword_remover():
  stopword_remover = None

  with open('static/pickle/stopword_remover.pkl', 'rb') as file:
    stopword_remover = pickle.load(file)

  return stopword_remover

def removeStopWords(text):
  stopword_remover = get_stopword_remover()

  stop = stopword_remover.remove(text)
  token = word_tokenize(stop)
  return ' '.join(token)

def splitCoincideWords(review):
  split_words = review.split()
  
  for idx, word in enumerate(split_words):
    w_len = len(word)

    if(w_len%2==0 and w_len>4):
      w_len = int(w_len/2)
      
      if(word[0:w_len] == word[w_len:]):
        split_words[idx] = word[0:w_len] + ' ' + word[w_len:]
              
  return ' '.join(split_words)

def get_stemmer():
  stemmer = None

  with open('static/pickle/stemmer.pkl', 'rb') as file:
    stemmer = pickle.load(file)

  return stemmer

def stem_text(text):
  stemmer=get_stemmer()

  return stemmer.stem(text)

def get_tokenizer():
  tokenizer = None

  with open('static/pickle/tokenizer.pkl', 'rb') as file:
    tokenizer = pickle.load(file)

  return tokenizer

def get_pad_sequence(preprocessed_comment):
  tokenizer = get_tokenizer()

  sequenced_comment = tokenizer.texts_to_sequences([preprocessed_comment])
  return pad_sequences(sequenced_comment, padding='post', truncating='post', maxlen=30)


def fire_text_preprocessing(text):
  text = remove_emoji(text)
  text = remove_link(text)
  text = replace_new_line_with_space(text)
  text = remove_html_tag(text)
  text = remove_extra_spaces(text)
  text = remove_special_char(text)
  text = remove_number(text)
  text = fix_review_with_punctuation(text)
  text = casefolding(text)
  text = splitCoincideWords(text)
  text = convertNonStandardWords(text)
  text = removeStopWords(text)
  text = stem_text(text)

  return get_pad_sequence(text)