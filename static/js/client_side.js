$(document).ready(function(){

  predict_sentiment_btn = $('#predict_sentiment')
  
  predict_sentiment_btn.click(function(e){
    e.preventDefault()

    let comment_input = $('#comment').val()
    let rating_input = $('#rating').find(':selected').val() //get option selected text

    if(isTextEmpty(comment_input) || isTextEmpty(rating_input)){
      showMessageModel("Invalid Input", "Form komentar dan/atau rating kosong!")
    }else if(!is_text_more_than_3_char(comment_input)){
      showMessageModel("Invalid Input", "Komentar tidak merepresentasikan suatu sentimen")
    }else{
      progress_circle = $('#progress_circle')

      progress_circle.css('display', 'inline-block')
      predict_sentiment_btn.attr('disabled', true)
      setTimeout(function(){
        try{
          $.ajax({
            url: '/api/predict_sentiment',
            type: 'POST',
            data:{
              'comment': comment_input
            },
            success: function(res){
              console.log(res)
              sentiment_result = res.sentiment

              showMessageModel("Hasil Kesesuaian Input", getModalResponse(rating_input, sentiment_result))
              progress_circle.css('display', 'none')
              predict_sentiment_btn.attr('disabled', false)
            }
          })
        }catch(e){
          console.log(e)
        }
      }, 2000)
    }
  })
})

function isTextEmpty(text){
  return text.trim() == ''
}

function is_text_more_than_3_char(text){
  return text.length > 3
}

function showMessageModel(modal_title, message){

  $('#modal-title').text(modal_title)
  $('#modal-message').text(message)
  $('#sentimentModal').modal('show')

}

function convertRatingToSentiment(rating){
  return rating<=3? "Negatif":"Positif"
}

function getModalResponse(rating_input, sentiment_predicted){
  return convertRatingToSentiment(rating_input)==sentiment_predicted? "Sesuai":"Tidak Sesuai"
}