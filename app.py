from flask import Flask, render_template, request, jsonify
from text_preprocess import fire_text_preprocessing
from predict import get_prediction_res

app = Flask(__name__, static_url_path='/static')

app.config["TEMPLATES_AUTO_RELOAD"] = True

model = None

@app.route("/")
def beranda():
    return render_template('index.html')

@app.route("/api/predict_sentiment", methods=['POST'])
def predictSentiment():
    comment = request.form['comment']

    preprocessed_comment = fire_text_preprocessing(comment)

    predict_sentiment_res = get_prediction_res(preprocessed_comment)
    
    return jsonify({
        'sentiment': predict_sentiment_res
    })

if __name__ == '__main__':

    # Run Flask di localhost
    app.run(host="localhost", port=5555, debug=True)
