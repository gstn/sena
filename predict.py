from keras.models import load_model

model = load_model('static/model/model_final.h5')

def get_prediction_res(padded_comment):
    pred_prob = model.predict(padded_comment)

    return "Positif" if pred_prob.tolist()[0][0]>0.5 else "Negatif"